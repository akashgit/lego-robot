import sheffield.*;
import icommand.nxt.*;
import icommand.nxt.comm.NXTCommand;

public class Barcode
{
  int[] lightReadings;

  public Barcode() throws InterruptedException
  {
    this(200);
  }
  
  public Barcode(int length) throws InterruptedException
  {
    lightReadings = new int[length];
  }
  
  public void save(String name)
  {
    if(name.length() > 0)
    {
      EasyWriter writer = new EasyWriter(name);
      
      for(int i = 0; i < lightReadings.length; i++)
        writer.println(lightReadings[i]);
    }
    else
      System.out.println("Wrong name of file");
  }
  
  public void load(String name)
  {
    if(name.length() > 0)
    {
      EasyReader reader = new EasyReader(name);
      
      for(int i = 0; i < lightReadings.length; i++)
        lightReadings[i] = reader.readInt();
    }
    else
      System.out.println("Wrong name of file");
  }
  
  public void setValue(int index, int value)
  {
    lightReadings[index] = value;
  }
  
  public int getValue(int index)
  {
    return lightReadings[index];
  }
  
  public int getLength()
  {
    return lightReadings.length;
  }
  
  public int decode() throws InterruptedException
  {
    int colourEdge = 650;
    
    int speedReadCoeficient = 57000;
    
    LightSensor lightSensor = new LightSensor(SensorPort.S3);
    lightSensor.activate();
    
    int readingSpeed = speedReadCoeficient / lightReadings.length;
    int nonReadingSpeed = readingSpeed * 4;
    
    Motor.A.setSpeed(readingSpeed);
    Motor.A.backward();
  
    for(int i = 0; i < lightReadings.length; i++)
    {
      int lightValue = lightSensor.getLightValue();
      //System.out.print(lightSensor.getLightPercent() + "\t");
      int isBar = lightValue < colourEdge ? 1 : 0;
      
      lightReadings[i] = isBar;
    }
    
    Motor.A.stop();
    
    String code = "";
    
    for(int i = 0; i < lightReadings.length; i++)
    {
      if(lightReadings[i] == 0)
        code += " ";
      else
        code += "1";
    }
    code = code.trim();
    
    String code2 = "";
    for(int i = 0; i < code.length(); i++)
    {
      if(code.charAt(i) == ' ')
        code2 += "1";
      else
        code2 += " ";
    }
    code2 = code2.trim();
    
    int bars = 1;
    int normalizedWidth = Math.round(code2.length() / 13);
    String bytecodeSequence = "";
    int numONE = 0;
    int numZERO = 0;
    for(int i = 0; i < code2.length(); i++)
    {
      if(bars < 14)
      {
        if(i % normalizedWidth == 0)
        {
          if(bars % 2 == 0)
          {
            String str = code2.substring(i, (i+normalizedWidth));
            System.out.print(str);
            System.out.println("|");
            
            for(int j = 0; j < str.length(); j++)
            {
              if(str.charAt(j) == ' ')
                numONE++;
              else
                numZERO++;
            }
            
            bytecodeSequence += numONE > numZERO ? "1" : "0";
          }
      
          bars++;
          numONE = numZERO = 0;
        }
      }
    }
    
    System.out.println(bytecodeSequence);
    
    int decodedNumber = -1;
    if(bytecodeSequence.equals("001101")) decodedNumber = 0;
    else if(bytecodeSequence.equals("011001")) decodedNumber = 1;
    else if(bytecodeSequence.equals("010011")) decodedNumber = 2;
    else if(bytecodeSequence.equals("111101")) decodedNumber = 3;
    else if(bytecodeSequence.equals("100011")) decodedNumber = 4;
    else if(bytecodeSequence.equals("110001")) decodedNumber = 5;
    else if(bytecodeSequence.equals("101111")) decodedNumber = 6;
    else if(bytecodeSequence.equals("111011")) decodedNumber = 7;
    else if(bytecodeSequence.equals("110111")) decodedNumber = 8;
    else if(bytecodeSequence.equals("001011")) decodedNumber = 9;                   
    
    return decodedNumber;   
  }
}