import sheffield.*;
import icommand.nxt.*;
import icommand.nxt.comm.NXTCommand;

public class BarcodeDisplay
{
  private EasyGraphics graphicsWindow;
  
  public BarcodeDisplay()
  {
    this(400, 80);
  }
  
  public BarcodeDisplay(int width, int height)
  {
    graphicsWindow = new EasyGraphics(width, height);
  }
  
  public void display(Barcode barcode)
  {
    for(int i = 0; i < barcode.getLength(); i++)
    {
      int color = 255 * barcode.getValue(i);
    
      graphicsWindow.setColor(color, color, color);  
      graphicsWindow.drawRectangle(i, 0, 1, 80);
    }
  }
}