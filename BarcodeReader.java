import icommand.nxt.*;
import icommand.nxt.comm.NXTCommand;

public class BarcodeReader
{
  public static void main(String[] args) throws InterruptedException
  {
    Robot readerBot = new Robot();
    BarcodeDisplay display = new BarcodeDisplay();
    display.display(readerBot.scan());
  }
}