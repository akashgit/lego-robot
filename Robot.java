import icommand.nxt.*;
import icommand.nxt.comm.NXTCommand;

public class Robot
{ 
  public Robot() throws InterruptedException
  {
    NXTCommand.open();
    NXTCommand.setVerify(true);
  }
  
  public void shutdown()
  {
    NXTCommand.close();
  }
  
  public Barcode scan() throws InterruptedException
  {
    LightSensor lightSensor = new LightSensor(SensorPort.S3);
    lightSensor.activate();
    
    Motor.A.setSpeed(360);
    Motor.A.backward();
    
    boolean barcodeFound = false;
    while(!barcodeFound)
    {
      if(lightSensor.getLightValue() < 600)
      {
        barcodeFound = true;
        Motor.A.stop();
        Motor.A.forward();
        Thread.sleep(500);
        Motor.A.stop();
      }
    }
    
    Barcode barcode = new Barcode(500);
    System.out.println(barcode.decode());
    barcode.save("lightValues.txt");
    
    lightSensor.passivate();
    
    return barcode;
  }
  
  public static void main(String[] args)
  {
  
  }
}